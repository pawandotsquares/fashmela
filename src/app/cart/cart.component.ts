import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartData: object;
  message: object;
	emptyMessage: string;
  userId: any;
	purchaseType: any;
	totalShippingCharge: number;
  quantity: any;
  maxQuantity: any;
  finaPriceValue: number;
  finaMrpValue: number;
  cartDataCount: number;
  discount: number;
  discountPercent: any;

  public spinnerConfig: any = {
	bdColor: 'rgba(51,51,51,0.8)',
	size: 'large',
	color: '#fff',
	type: 'ball-circus',
	loadigText: 'Loading...'
  };
  constructor(
	private DataService: DataService,
	private spinner: NgxSpinnerService,
	private router : Router
  ) { }

  ngOnInit() {
	this.spinner.show();
	this.purchaseType = localStorage.getItem('purchaseType');
	this.userId = localStorage.getItem('userId');
	this.cartList(this.userId, this.purchaseType);
	this.cartCount(this.userId);
  }
  cartList(userId, purchaseType) {
	this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {

		this.spinner.hide();
	  if (resultCartData.statusCode == 200) {
		  this.cartData = resultCartData.responsePacket;
		  /* resultCartData.responsePacket.map(obj => {
		  this.finaPriceValue = obj.product.totalPrice;
		  this.finaPriceValue = this.finaPriceValue + obj.product.totalPrice;

		  this.finaMrpValue = obj.product.mrp;
		  this.finaMrpValue = this.finaMrpValue + obj.product.mrp;
		});
		if (this.finaMrpValue > this.finaPriceValue ){
		  this.discount = this.finaMrpValue - this.finaPriceValue;
		  this.discountPercent = (this.discount / this.finaMrpValue)*100
		}else{
		  this.discount = 0;
		} */
		} else if (resultCartData.statusCode == 202) {
			localStorage.clear()
			alert("Your account has been deactivated, Please login again to proceed");
			this.router.navigateByUrl("/login");
	  } else {
		this.message = resultCartData.message;
		this.emptyMessage = "Your Shopping Cart is empty";
	  }
	}
	)
  }
  cartCount(userId) {
	this.spinner.show();
	this.DataService.getCartCount(userId).subscribe(resultCartCount => {
	  this.spinner.hide();
	  if (resultCartCount.statusCode == 200) {
		this.cartDataCount = resultCartCount.responsePacket.itemCount;
			this.finaMrpValue = resultCartCount.responsePacket.totalMrp;
			this.totalShippingCharge = resultCartCount.responsePacket.totalShippingCharge;
			this.finaPriceValue = resultCartCount.responsePacket.total_price;

			if (this.totalShippingCharge) {
				this.finaPriceValue = this.finaPriceValue + this.totalShippingCharge
			}

		if (this.finaMrpValue > this.finaPriceValue) {
			this.discount = this.finaMrpValue - this.finaPriceValue;
		} else {
			this.discount = 0;
		}
		this.discountPercent = resultCartCount.responsePacket.totalDiscountPercentage;

	  } else {
		this.message = resultCartCount.message;
	  }
	})
  }
  deleteCartProduct(productCartId) {
	this.spinner.show();
	this.DataService.removeCartItem(productCartId).subscribe(resultCartData => {
		this.spinner.hide();
	  if (resultCartData.statusCode == 200) {
			this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
			this.router.navigate(["cart"]));
			this.ngOnInit();
	  }else{
		  this.message = resultCartData.message;
			this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
			this.router.navigate(["cart"]));
	  }
	 })
  }

	qtyDecr(cart) {
		if (cart.quantity > 1) {
		this.spinner.show();
		let updateToCartObj = {};
		updateToCartObj = {
			cartId: cart.cartId,
			productCartId: cart.productCartId,
			quantity: cart.quantity - 1,
			product: cart.product
		}
			updateToCartObj = JSON.stringify(updateToCartObj)
			this.DataService.updateCartCount(updateToCartObj).subscribe(result => {
				if (result.statusCode == 200) {
					this.spinner.hide();
					this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
						this.router.navigate(["cart"]));
				}
			})
		}
  }

	qtyIncr(cart) {
		if (cart.quantity < cart.product.quantity) {
		this.spinner.show();
		let updateToCartObj = {};
		updateToCartObj = {
			cartId: cart.cartId,
			productCartId: cart.productCartId,
			quantity: cart.quantity + 1,
			product: cart.product
		}
		updateToCartObj = JSON.stringify(updateToCartObj)
			this.DataService.updateCartCount(updateToCartObj).subscribe(result => {
				if (result.statusCode == 200) {
					this.spinner.hide();
					this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
						this.router.navigate(["cart"]));
				}
			})
	} else {
	  alert("You have exceeded the max quantity for this item");
	}
  }
}
