import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  purchaseType : any;
  searchProductForm: FormGroup;
  submitted = false;
  message: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {

    this.searchProductForm = this.formBuilder.group({
      search: ['']
    });

    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType');
    }else{
      this.purchaseType = 1;
    }
  }
  get f() {

    return this.searchProductForm.controls;

  }
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.searchProductForm.invalid) {
      return;
    }

    let searchString = this.f.search.value;

    if (this.f.search.value) {

      this.router.navigate(['/product','search'], { queryParams: { search: searchString } });
      //this.router.navigateByUrl('/profile');

    } else {
      alert('Please enter product name to search');
    }

  }

}
