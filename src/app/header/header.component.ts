import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn:any

  constructor(
    private authService: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    if (this.isLoggedIn == 'true'){
      this.isLoggedIn = 'true';
    }else{
      this.isLoggedIn = 'false';
    }
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
