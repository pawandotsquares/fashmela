import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-address',
  templateUrl: './list-address.component.html',
  styleUrls: ['./list-address.component.css']
})
export class ListAddressComponent implements OnInit {
  addressData: object;
  message: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService,
    private Router: Router
  ) { }

  ngOnInit() {
    this.spinner.show();
    let userId = localStorage.getItem('userId');

    this.allAddress(userId);
  }
  allAddress(userId) {
    this.dataservice.getUsersAddress(userId).subscribe(resultAddressData => {
      this.spinner.hide();
      if (resultAddressData.statusCode == 200) {
        this.addressData = resultAddressData.responsePacket
      } else {
        this.message = resultAddressData;
        localStorage.setItem('statusCode', resultAddressData.message);
      }
    },
      error => {

      })
  }
  deleteAddress(selectedAddressId){
    this.spinner.show();
    this.dataservice.deleteUserAddress(selectedAddressId).subscribe(resultAddressData => {
      //console.log(resultAddressData);
      this.spinner.hide();
      if (resultAddressData.statusCode == 200) {
        this.Router.navigateByUrl('', { skipLocationChange: true }).then(() =>
        this.Router.navigate(["view-address"]));
      } else {
        this.message = resultAddressData;
      }
    },
      error => {

      })
  }
  selectedAddress(selectedAddressId){
    localStorage.setItem('addressId', "");
    localStorage.setItem('addressId', selectedAddressId);
    this.Router.navigateByUrl('/checkout');
  }
}
