import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { observable } from "rxjs";
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  purchaseType: any;
  products: object;
  categoryList: object;
  trendingproduct: object;
  slides: object;
  slideConfig: object;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
    ) {
    this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));
  }

  getProductByPurchaseType(id: any) {
    if (id && id == 1 || id == 2) {
      localStorage.setItem('purchaseType', id);
    }
  }


  ngOnInit() {

    this.spinner.show();

    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType')
    }else{
      this.purchaseType = 1;
    }
    this.categories()
    this.featuredProduct(this.purchaseType)
    this.trendingProducts(this.purchaseType)
    this.detectmob();

  }

  featuredProduct(purchaseType:any) {
    this.dataservice.getFeaturedProducts(purchaseType).subscribe(resultFeaturedProduct => {
      this.spinner.hide();
      this.products = resultFeaturedProduct.responsePacket
    },
      error => {

      })
  }

  categories() {
    this.dataservice.getCategories().subscribe(resultCategories => {
      this.spinner.hide();
      this.categoryList = resultCategories.responsePacket
    },
      error => {

      })
  }
  detectmob() {
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
      },
      any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
    };

    if (isMobile.any()) {
      this.slideConfig = {
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "arrows": true,
        "prevArrow": '<button type="button" class="slick-prev"><img src="assets/images/arow-left.png"></button>',
        "nextArrow": '<button type="button" class="slick-next"><img src="assets/images/arow-right.png"></button>',
      };
    }else{
      this.slideConfig = {
        "slidesToShow": 3,
        "slidesToScroll": 3,
        "arrows": true,
        "prevArrow": '<button type="button" class="slick-prev"><img src="assets/images/arow-left.png"></button>',
        "nextArrow": '<button type="button" class="slick-next"><img src="assets/images/arow-right.png"></button>',
      };
    }
  }
  trendingProducts(purchaseType: any) {
    this.dataservice.getTrendingProducts(purchaseType).subscribe(resultTrendingProduct => {
      this.spinner.hide();
      this.trendingproduct = resultTrendingProduct.responsePacket
    },
      error => {

      })
  }
}
