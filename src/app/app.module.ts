import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { SearchComponent } from './search/search.component';
import { SocialComponent } from './social/social.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './register/register.component';
import { IndexComponent } from './index/index.component';
import { HttpClientModule } from '@angular/common/http';
import { SlickModule } from 'ngx-slick';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AlertComponent } from './alert/alert.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ProductComponent } from './product/product.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { CategoriesComponent } from './categories/categories.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { WholesaleComponent } from './wholesale/wholesale.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ProfileSidebarComponent } from './profile-sidebar/profile-sidebar.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AddressComponent } from './address/address.component';
import { WalletComponent } from './wallet/wallet.component';
import { ContactComponent } from './contact/contact.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermandconditionComponent } from './termandcondition/termandcondition.component';
import { ReturnComponent } from './return/return.component';
import { DeliveryinfoComponent } from './deliveryinfo/deliveryinfo.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { CartPopupComponent } from './cart-popup/cart-popup.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { ListAddressComponent } from './list-address/list-address.component';
import { PaymentComponent } from './payment/payment.component';
import { SuccessPaymentComponent } from './success-payment/success-payment.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';
import { InputTrimModule } from 'ng2-trim-directive';

import {
  Ng6SocialButtonModule,
  SocialServiceConfig
} from "ng6-social-button";
import { BusinessProfileComponent } from './business-profile/business-profile.component';
import { EditBusinessProfileComponent } from './edit-business-profile/edit-business-profile.component';
import { AddBusinessProfileComponent } from './add-business-profile/add-business-profile.component';
import { PrivacyComponent } from './privacy/privacy.component';
// Configs
export function getAuthServiceConfigs() {
  let config = new SocialServiceConfig()
    .addFacebook("2141198699233404")
    .addGoogle("Your-Google-Client-Id");
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    SearchComponent,
    SocialComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    IndexComponent,
    AlertComponent,
    ProfileComponent,
    EditProfileComponent,
    ProductComponent,
    ProductdetailsComponent,
    CategoriesComponent,
    WholesaleComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    ProfileSidebarComponent,
    ChangepasswordComponent,
    AddressComponent,
    WalletComponent,
    ContactComponent,
    AboutusComponent,
    PrivacypolicyComponent,
    TermandconditionComponent,
    ReturnComponent,
    DeliveryinfoComponent,
    OrderHistoryComponent,
    OrderDetailsComponent,
    WalletHistoryComponent,
    CartPopupComponent,
    CartComponent,
    CheckoutComponent,
    AddAddressComponent,
    EditAddressComponent,
    ListAddressComponent,
    PaymentComponent,
    SuccessPaymentComponent,
    PaymentFailedComponent,
    BusinessProfileComponent,
    EditBusinessProfileComponent,
    AddBusinessProfileComponent,
    PrivacyComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    SlickModule.forRoot(),
    NgxGalleryModule,
    NgxSpinnerModule,
    Ng6SocialButtonModule,
    InputTrimModule
  ],
  providers: [
    {
      provide: SocialServiceConfig,
      useFactory: getAuthServiceConfigs
    },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
