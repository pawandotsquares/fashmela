import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DataService } from "../data.service";
import { Md5 } from "ts-md5/dist/md5";
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-business-profile',
  templateUrl: './edit-business-profile.component.html',
  styleUrls: ['./edit-business-profile.component.css']
})
export class EditBusinessProfileComponent implements OnInit {
  editBusinessForm: FormGroup;
  message: object;
  profileDataEdit: object;
  submitted = false;
  profileData: any;
  firstName: any;
  base64textString: any;
  editFromimg: any;
  base64textStringForSave: any;
  imgFlag: boolean;
  noImgFlag: boolean;
  base64ImgFlag: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private FormBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) {

    this.route.params.subscribe(params => this.getBusinessprofileDetailsByID(localStorage.getItem('userId')));
  }

  getBusinessprofileDetailsByID(id: any) {

    this.DataService.getBusinessProfile(id).subscribe(resultProfileDetails => {
      console.log(resultProfileDetails);
      var editBusinessFormArr = {};
      this.spinner.hide();
      if (resultProfileDetails.statusCode == 200) {
        editBusinessFormArr = {
          userId: localStorage.getItem('userId'),
          businessProfileId: resultProfileDetails.responsePacket.businessProfileId,
          companyName: resultProfileDetails.responsePacket.companyName,
          holderName: resultProfileDetails.responsePacket.holderName,
          accountNumber: resultProfileDetails.responsePacket.accountNumber,
          ifscCode: resultProfileDetails.responsePacket.ifscCode
        };

        this.editBusinessForm.setValue(editBusinessFormArr);
        this.profileData = resultProfileDetails.responsePacket;
        this.profileDataEdit = resultProfileDetails.responsePacket
      }
    }

    )
  }
  ngOnInit() {

    this.spinner.show();
    this.editBusinessForm = this.FormBuilder.group({
      userId: [''],
      businessProfileId: [''],
      companyName: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      accountNumber: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      holderName: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      ifscCode: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],

    });
  }
  get f() {
    return this.editBusinessForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editBusinessForm.invalid) {
      return;
    }
    this.spinner.show();

    let editBusinessFormValue = this.editBusinessForm.value;

    //call login api here
    this.DataService.updateBusinessDetails(this.editBusinessForm.value, this.base64textStringForSave).subscribe(resultCategories => {
      this.spinner.hide();
      if (resultCategories.statusCode == 200) {
        this.Router.navigateByUrl('/business-profile');
      } else {
        this.message = resultCategories;
      }
    },
      error => {

      })

  }
  onKey(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
