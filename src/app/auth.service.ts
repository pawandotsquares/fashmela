import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.setItem('isLoggedIn', "false");
    localStorage.clear()
    /* localStorage.clear(); */
  }
}
