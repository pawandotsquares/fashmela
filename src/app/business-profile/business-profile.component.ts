import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-business-profile',
  templateUrl: './business-profile.component.html',
  styleUrls: ['./business-profile.component.css']
})
export class BusinessProfileComponent implements OnInit {
  profileData: object;
  message: any;
  noData: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.noData = true;;
    let userId = localStorage.getItem('userId');
    this.spinner.show();
    this.businessProfileDetails(userId)
  }
  businessProfileDetails(userId) {
    this.dataservice.getBusinessProfile(userId).subscribe(resultProfileData => {
      this.spinner.hide();
      if (resultProfileData.statusCode == 200) {
        if (resultProfileData.responsePacket.profileImage) {
          this.noData = false;
        }
        this.profileData = resultProfileData.responsePacket
      } else {
        this.message = resultProfileData;
        localStorage.setItem('statusCode', resultProfileData.message);
      }
    },
      error => {

      })
  }
}
