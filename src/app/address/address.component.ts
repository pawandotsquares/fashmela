import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  addressData: object;
  message: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    let userId = localStorage.getItem('userId');

    this.allAddress(userId);
  }
  allAddress(userId) {
    this.dataservice.getUsersAddress(userId).subscribe(resultAddressData => {
      console.log(resultAddressData);
      this.spinner.hide();
      if (resultAddressData.statusCode == 200) {
        this.addressData = resultAddressData.responsePacket
      } else {
        this.message = resultAddressData;
        localStorage.setItem('statusCode', resultAddressData.message);
      }
    },
      error => {

      })
  }
}
