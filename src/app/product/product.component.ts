import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productData: object;
  productListData: object;
  message: object;
  purchaseType: any;
  searchParam: string;

  constructor(
    private dataservice: DataService,
    private route:ActivatedRoute,
    private router:Router,
    private spinner: NgxSpinnerService
  ) {
    this.route.params.subscribe(params => this.getProductListByID(params['id']));
  }
  getProductListByID(id: number) {
    if (!isNaN(id)) {
      this.purchaseType = localStorage.getItem('purchaseType');
      this.dataservice.getProductListById(id, this.purchaseType).subscribe(resultproductDetails => {
        if (resultproductDetails.statusCode == 200) {
          this.spinner.hide();
          this.productListData = resultproductDetails.responsePacket.productList;
        }else{
          this.message = resultproductDetails.message;
          this.spinner.hide();
        }
      })
    }

  }
  ngOnInit() {
    this.spinner.show();
    this.searchParam = this.route.snapshot.queryParamMap.get("search")
    this.purchaseType = localStorage.getItem('purchaseType');
    if (this.searchParam){
      this.dataservice.getProductListBySearch(this.searchParam, this.purchaseType).subscribe(resultproductDetails => {

        if (resultproductDetails.statusCode == 200) {
          this.productListData = resultproductDetails.responsePacket.productList;
          this.spinner.hide();

        }else{
          this.message = resultproductDetails.message;
          this.spinner.hide();
        }
      })
    }else{
      if (this.searchParam!=null) {
        this.router.navigateByUrl('/');
      }
    }
  }
}
