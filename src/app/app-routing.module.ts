import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { IndexComponent } from './index/index.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { AuthGuard } from './auth.guard';
import { ProductComponent } from './product/product.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { CategoriesComponent } from './categories/categories.component';
import { ContentComponent } from './content/content.component';
import { WholesaleComponent } from './wholesale/wholesale.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AddressComponent } from './address/address.component';
import { WalletComponent } from './wallet/wallet.component';
import { ContactComponent } from './contact/contact.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { DeliveryinfoComponent } from './deliveryinfo/deliveryinfo.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermandconditionComponent } from './termandcondition/termandcondition.component';
import { ReturnComponent } from './return/return.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { ListAddressComponent } from './list-address/list-address.component';
import { PaymentComponent } from './payment/payment.component';
import { SuccessPaymentComponent } from './success-payment/success-payment.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';
import { BusinessProfileComponent } from './business-profile/business-profile.component';
import { EditBusinessProfileComponent } from './edit-business-profile/edit-business-profile.component';
import { AddBusinessProfileComponent } from './add-business-profile/add-business-profile.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  { path: 'address', component: AddressComponent, canActivate: [AuthGuard] },
  { path: 'add-address', component: AddAddressComponent, canActivate: [AuthGuard] },
  { path: 'about', component: AboutusComponent },
  { path: 'add-business-profile', component: AddBusinessProfileComponent, canActivate: [AuthGuard] },
  { path: 'business-profile', component: BusinessProfileComponent, canActivate: [AuthGuard]},
  { path: 'edit-business-profile', component: EditBusinessProfileComponent, canActivate: [AuthGuard] },
  { path: 'edit-address/:id', component: EditAddressComponent, canActivate: [AuthGuard] },
  { path: 'editprofile', component: EditProfileComponent, canActivate: [AuthGuard] },
  { path: 'categories', component: CategoriesComponent },
  { path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard] },
  { path: 'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
  { path: 'contact', component: ContactComponent },
  { path: 'cart', component: CartComponent, canActivate:[AuthGuard] },
  { path: 'deliveryinfo', component: DeliveryinfoComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'login', component: LoginComponent },
  { path: 'order-history', component: OrderHistoryComponent, canActivate: [AuthGuard] },
  { path: 'order-details/:id', component: OrderDetailsComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'payment-success', component: SuccessPaymentComponent },
  { path: 'payment-failure', component: PaymentFailedComponent },
  { path: 'payment', component: PaymentComponent},
  { path: 'privacy-policy', component: PrivacypolicyComponent },
  { path: 'privacypolicy', component: PrivacyComponent },
  { path: 'product/:id', component: ProductComponent},
  { path: 'product?search=name', component: ProductComponent},
  { path: 'productdetails/:id', component: ProductdetailsComponent },
  { path: 'resetpassword/:id', component: ResetpasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'return-policy', component: ReturnComponent },
  { path: 'singlestore/:id', component: IndexComponent,},
  { path: 'view-address', component: ListAddressComponent, canActivate: [AuthGuard] },
  { path: 'wholesale/:id', component: WholesaleComponent },
  { path: 'wallet', component: WalletComponent, canActivate: [AuthGuard]  },
  { path: 'wallet-history', component: WalletHistoryComponent, canActivate: [AuthGuard]  },
  { path: 'terms-conditions', component: TermandconditionComponent  },
  { path: '', component: IndexComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
 /*  imports: [
    CommonModule
  ], */
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }


