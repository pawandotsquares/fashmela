import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { RequestOptions } from '@angular/http';
/* const apiUrl = 'https://rebbu47ee5.execute-api.ap-south-1.amazonaws.com/uat';
const apiUrl2 = 'https://qmh3qmfp2j.execute-api.ap-south-1.amazonaws.com/uat-next'; */
const apiUrl = 'https://kwc47bogoc.execute-api.ap-south-1.amazonaws.com/production';
const apiUrl2 = 'https://ad64rhtk3k.execute-api.ap-south-1.amazonaws.com/production-next';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getFeaturedProducts(purchaseType) {
    return this.http.post(apiUrl + '/get-featured-products', { productType: purchaseType }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getTrendingProducts(purchaseType) {
    return this.http.post(apiUrl + '/get-trending-products', { productType: purchaseType}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCategories() {
    return this.http.post(apiUrl +'/getcategorylist',{}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }


  userlogin(email, password) {
    return this.http.post(apiUrl+'/auth', { email: email,password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  /* register(email, password) {
    return this.http.post(apiUrl+'/auth', { email: email, password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */

  register(user, password) {
    return this.http.post(apiUrl+`/user-register`, {
      "firstName": user.name, "password": password, "email": user.email, "phoneNumber": user.mobile_number, "address": user.address, "userType": user.userType }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  logout(): void {
    localStorage.setItem('isLoggedIn', "false");
    localStorage.removeItem('email');
  }

	getUserProfile(userId) {
		return this.http.post(apiUrl+`/get-user-profile`, { 'userId': userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
	));
}
	getBusinessProfile(userId) {
    return this.http.post(apiUrl +`/get-business-profile`, { 'userId': userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
	));
}
  updateUserProfile(user, base64Img) {
    return this.http.post(apiUrl+`/user-register`, {
      "userId": user.userId, "firstName": user.firstName, "password": user.password, "email": user.email, "phoneNumber": user.phoneNumber, "address": user.address, 'profileImage': base64Img
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  updateBusinessDetails(user, base64Img) {
    return this.http.post(apiUrl +`/save-business-profile`, {
      "businessProfileId": user.businessProfileId, "companyName": user.companyName, "holderName": user.holderName, "accountNumber": user.accountNumber, "ifscCode": user.ifscCode, "user": { "userId": user.userId, "isActive": false}
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addBusinessDetails(userJson, user) {
    return this.http.post(apiUrl +`/save-business-profile`, {
      "businessProfileId": 0, "companyName": userJson.companyName, "holderName": userJson.holderName, "accountNumber": userJson.accountNumber, "ifscCode": userJson.ifscCode, user
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductList() {
    return this.http.post(apiUrl+`/getproductlistv1`, { "categoryId": 1, "productType": 1, "pageNumber": 1, "pageSize": 10, "searchKeyword": ""}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductListById(catId, purchaseType) {
    return this.http.post(apiUrl + `/getproductlistv1`, { "categoryId": catId, "productType": purchaseType, "pageNumber": 1, "pageSize": 10, "searchKeyword": "" }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductListBySearch(searchParam, purchaseType) {
    return this.http.post(apiUrl + `/getproductlistv1`, { "searchKeyword": searchParam, "productType": purchaseType, "pageNumber": 1, "pageSize": 10 }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  //, purchaseType
  //, "purchaseType": purchaseType
  getProductDetails(productId, purchaseType) {
    return this.http.post(apiUrl +`/getproductdetails`, { "productId": productId, "purchaseType": purchaseType }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  forgotPass(emailId) {
    return this.http.post(apiUrl+`/forgot-password`, { "email": emailId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  resetPass(token, newPassword) {
    return this.http.post(apiUrl+`/forgot-password`, { "token": token, "newPassword": newPassword }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  changePass(userName, oldPassword, newPassword) {
    return this.http.post(apiUrl+`/change-password`, { "userName": userName, "oldPassword": oldPassword, "newPassword": newPassword, "isEmailAsUserName":true }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getUsersAddress(userId) {
    return this.http.post(apiUrl+`/get-delivery-address-list`, { "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getAddressById(deliveryAddressId) {
    return this.http.post(apiUrl + `/get-delivery-address`, { "deliveryAddressId": deliveryAddressId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  adduserAddress(addressData, userId) {
    return this.http.post(apiUrl+`/save-user-delivery-address`, {
      "deliveryAddressId": addressData.deliveryAddressId, "fullName": addressData.name, "mobileNumber": addressData.mobile_number, "pincode": addressData.pin, "addressLine1": addressData.building, "addressLine2": addressData.locality, "city": addressData.city, "state": addressData.state, "fromName": addressData.fromName, "fromContactNumber": addressData.fromContactNumber, "fromAddress": addressData.fromAddress, "user": { "userId": userId }
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
 /*  deleteUserAddress(deliveryAddressId) {
    return this.http.post(apiUrl+`/delete-user-delivery-address`, {
      "deliveryAddressId": deliveryAddressId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */
  deleteUserAddress(deliveryAddressId) {

    let body = JSON.stringify(
      {
        deliveryAddressId
      }
    );

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
      })
      ,
      body: body
    };

    return this.http.delete(apiUrl + `/delete-user-delivery-address`, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  sendContactQuery(user) {
    return this.http.post(apiUrl2 + `/save-user-feedback`, {
      "name": user.name, "email": user.email, "subject": user.subject, "message": user.message}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  walletAmount(userId) {
    return this.http.post(apiUrl+`/get-user-wallet`, {
      "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  walletHistory(userId) {
    return this.http.post(apiUrl2 + `/get-user-wallet-history`, {
      "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  orderHistory(userId) {
    return this.http.post(apiUrl+`/order-history-v1`, {
      "userId": userId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  orderDetails(orderUuId) {
    return this.http.post(apiUrl+`/get-order-details`, {
      "orderUuId": orderUuId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  addToCart(data) {
    return this.http.post(apiUrl+`/add-to-cart`,
      data
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCart(userId, purchaseType) {
    return this.http.post(apiUrl+`/get-cart-items`, {
      "userId": userId, "purchaseType": purchaseType
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCartCount(userId) {
    return this.http.post(apiUrl+`/get-cart-item-count`, {
      "userId": userId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  updateCartCount(data) {
    return this.http.put(apiUrl+`/update-cart-item-quantity`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  payNowData(data) {
    return this.http.post(apiUrl2 + `/proceed-payment`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  deductWalletAmount(userId, walletamount) {
    return this.http.post(apiUrl2 + `/apply-user-wallet-amount`,{
      userId: userId, amount: walletamount, isRemove:true
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  palceOrder(data) {
    return this.http.post(apiUrl + `/place-order`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  removeCartItem(productCartId) {

    let body = JSON.stringify(
      {
        productCartId
      }
    );

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
      })
      ,
      body: body
    };

    return this.http.delete(apiUrl+`/remove-cart-item`, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

}
