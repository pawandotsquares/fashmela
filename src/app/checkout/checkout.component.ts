import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public payuform: any = {};
  paymentForm: FormGroup;
  /* cartData: object; */
  cartData: any;
  message: object;
  addressData: object;
  userId: any;
  purchaseType: any;
  quantity: any;
  maxQuantity: any;
  paynow: boolean;
  paymentGatway: any;
  cartProductData: any;
  finalPriceValue: number;
  mainFinalPriceValue: number;
  totalShippingCharge: number;
  finalPrice: number;
  finaMrpValue: number;
  cartDataCount: number;
  discount: number;
  discountPercent: any;
  deliveryAddress: boolean;
  noDeliveryAddress: boolean;
  walletData: any;
  walletSelected: any;
  walletTotalAmount:number;
  walletDiducdedAmount:number;
  walletFlag:boolean;
  isCheckedVal:boolean;
  finalData:any;
  submitted = false;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private DataService: DataService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private http: Http,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.noDeliveryAddress = false
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    this.walletAmount(this.userId)
    this.purchaseType = localStorage.getItem('purchaseType');
    this.cartList(this.userId, this.purchaseType);
    this.cartCount(this.userId);
    const deliveryAddressId = localStorage.getItem('addressId')

    if (deliveryAddressId){
      this.getAddressById(deliveryAddressId)
    }else{
      this.allAddress(this.userId)
    }
  }
  cartList(userId, purchaseType) {
    this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {

      this.spinner.hide();
      var orderItems = [];
      if (resultCartData.statusCode == 200) {
        this.cartData = resultCartData.responsePacket;
        this.cartData.forEach(products => {
          let product = {
            "productId": products.product.productId,
            "totalPrice": products.product.totalPrice,
            "shippingCharge": 0,
            "courierPartner": "rocket mail",
            "quantity": products.quantity,
            "productType": products.product.purchaseType,
            "sizeList": products.product.sizeList,
          }
          orderItems.push(product);
        });
        this.cartProductData = orderItems;
      } else if (resultCartData.statusCode == 202) {
        alert("Your account has been deactivated, Please login again to proceed");
        this.router.navigateByUrl("/login");
      } else {
        this.router.navigate(["cart"]);
        this.message = resultCartData.message;
      }
    }
    )
  }
  cartCount(userId) {
    this.spinner.show();
    this.DataService.getCartCount(userId).subscribe(resultCartCount => {
      this.spinner.hide();
      console.log(resultCartCount);
      if (resultCartCount.statusCode == 200) {
        this.cartDataCount = resultCartCount.responsePacket.itemCount;
        this.finaMrpValue = resultCartCount.responsePacket.totalMrp;
        this.totalShippingCharge = resultCartCount.responsePacket.totalShippingCharge;
        this.finalPriceValue = resultCartCount.responsePacket.total_price;
        this.mainFinalPriceValue = resultCartCount.responsePacket.total_price;
        if (this.totalShippingCharge){
          this.finalPriceValue = this.finalPriceValue+this.totalShippingCharge
          this.mainFinalPriceValue = this.mainFinalPriceValue+this.totalShippingCharge
        }
        if (this.finaMrpValue > this.finalPriceValue) {
          this.discount = this.finaMrpValue - this.finalPriceValue;
        } else {
          this.discount = 0;
        }
        this.discountPercent = resultCartCount.responsePacket.totalDiscountPercentage;

      } else {
        this.message = resultCartCount.message;
      }
    })
  }
  walletAmount(userId) {
    this.walletFlag = false;
    this.DataService.walletAmount(userId).subscribe(resultWalletData => {
      if (resultWalletData.statusCode == 200) {
        this.walletData = resultWalletData.responsePacket
        this.walletTotalAmount = resultWalletData.responsePacket.amount
        if (this.walletTotalAmount>0){
          this.walletFlag = true;
        }
      } else {
        this.message = resultWalletData;
        localStorage.setItem('statusCode', resultWalletData.message);
      }
    },
      error => {

      })
  }
  allAddress(userId) {
    this.DataService.getUsersAddress(userId).subscribe(allAddressData => {
      this.spinner.hide();
      if (allAddressData.statusCode == 200) {
        localStorage.setItem('addressId', allAddressData.responsePacket[0].deliveryAddressId)
        this.addressData = allAddressData.responsePacket
        this.deliveryAddress = false;
        this.noDeliveryAddress = true;
      } else {
        this.noDeliveryAddress = false;
        this.message = allAddressData;
        localStorage.setItem('statusCode', allAddressData.message);
      }
    },
      error => {

      })
  }
  getAddressById(addressId) {
    this.DataService.getAddressById(addressId).subscribe(resultAddressData => {
      this.spinner.hide();
      if (resultAddressData.statusCode == 200) {
        localStorage.setItem('addressId',resultAddressData.responsePacket.deliveryAddressId)
        this.addressData = resultAddressData.responsePacket
        this.deliveryAddress = true;
        this.noDeliveryAddress = true;
      } else {
        this.message = resultAddressData;
        localStorage.setItem('statusCode', resultAddressData.message);
      }
    },
      error => {

      })
  }
  checkValue(event: any) {
    if (event == "A"){
      localStorage.setItem("walletSelected",'true');
      this.isCheckedVal = true;
    }else{
      localStorage.setItem("walletSelected", 'false');
      this.isCheckedVal = false;
    }

    if (localStorage.getItem("walletSelected") == "true") {

      if (this.walletTotalAmount < 100){
        if (this.walletTotalAmount < this.mainFinalPriceValue) {
          this.finalPrice = this.finalPriceValue - this.walletTotalAmount;
          this.walletDiducdedAmount = this.walletTotalAmount;
        } else {
          this.finalPrice = 0;
          this.walletDiducdedAmount = this.mainFinalPriceValue;
        }
      }else{
        if (this.mainFinalPriceValue > 100) {
          this.walletDiducdedAmount = 100;
          this.finalPrice = this.finalPriceValue - this.walletDiducdedAmount;
        } else {
          this.finalPrice = 0;
          this.walletDiducdedAmount =  this.mainFinalPriceValue;
        }
      }
    }else{
      this.finalPrice = this.mainFinalPriceValue;
      this.walletDiducdedAmount = 0;
    }

    this.finalPriceValue = this.finalPrice;

  }

  payNow(){
    if (this.walletDiducdedAmount){
      this.tempWalletAmount(this.userId, this.walletDiducdedAmount);
    }
    const paymentData = {
      amount: this.finalPriceValue,
      walletDiducdedAmount: this.walletDiducdedAmount,
      mainFinalPriceValue: this.mainFinalPriceValue,
    }
    localStorage.setItem('finalPaymentData', JSON.stringify(paymentData));
    let addressId = localStorage.getItem('addressId');
    if (this.noDeliveryAddress){
     if (this.finalPriceValue>0){
        this.router.navigate(["payment"]);
      }else{
        let palceOrderObj = {};

        palceOrderObj = {
          orderItems: this.cartProductData,
          user: {
            userId: localStorage.getItem('userId')
          },
          payment: {
            modeOfPayment: "",
            successUrl: "https://d3rc17ccvsmeqb.cloudfront.net/payment-success",
            failureUrl: "https://d3rc17ccvsmeqb.cloudfront.net/payment-failure",
            paymentGatway: "",
          },
          totalAmount: this.mainFinalPriceValue,
          walletAmountUsed: this.walletDiducdedAmount,
          paidAmount: this.finalPriceValue,
          address: {
            deliveryAddressId: localStorage.getItem('addressId'),
          },
        };
        palceOrderObj = JSON.stringify(palceOrderObj)
;
         this.DataService.palceOrder(palceOrderObj).subscribe(resultPaymentData => {
          if (resultPaymentData.statusCode == 200) {
            this.finalData = resultPaymentData.responsePacket
            localStorage.setItem('transaction_id', this.finalData.orderUuId)
            localStorage.setItem('freecheckout',"true"),
            this.router.navigate(["payment-success"]);
          } else if (resultPaymentData.statusCode == 202) {
            localStorage.clear()
            alert("Your account has been deactivated, Please login again to proceed");
            this.router.navigateByUrl("/login");
          } else {
            localStorage.setItem('freecheckout', "false"),
              this.router.navigate(["payment-failure"]);
          }
        },
          error => {
          })
      }
    }else{
      alert("Please select address");
    }

  }
  tempWalletAmount(userId, walletamount){
    this.DataService.deductWalletAmount(userId, walletamount).subscribe(resultWalData => {
      if (resultWalData.statusCode == 200) {
      } else {

      }
    },
      error => {
      })
  }
}
