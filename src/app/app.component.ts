import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fashmela E-Com';
  loading = false;
  returnUrl: string;
  private currenturl = '';


  constructor(private router: Router) { }

  ngOnInit() {

    this.getCurrentUrl();
  }

  // Get Current URL
  getCurrentUrl() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currenturl = event.url;
      }
    });
  }
}
