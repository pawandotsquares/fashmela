import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData: object;
  message: any;
	noImg: boolean;

	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};

  constructor(
	private dataservice: DataService,
	private spinner: NgxSpinnerService

  ) { }

  ngOnInit() {
		this.noImg = true;;
		let userId = localStorage.getItem('userId');
		this.spinner.show();
		this.profileDetails(userId)
  }
  profileDetails(userId){
	this.dataservice.getUserProfile(userId).subscribe(resultProfileData => {
		this.spinner.hide();
	  if (resultProfileData.statusCode == 200) {
		if (resultProfileData.responsePacket.profileImage){
		  this.noImg = false;
		}

		this.profileData = resultProfileData.responsePacket
	  } else {
		this.message = resultProfileData;
		localStorage.setItem('statusCode', resultProfileData.message);
	  }
	},
	  error => {

	  })
  }
}
