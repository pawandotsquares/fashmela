import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  orderData: object;
  message: any;
  noImg: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.noImg = true;;
    let userId = localStorage.getItem('userId');
    this.spinner.show();
    this.orderHistory(userId)
  }
  orderHistory(userId) {

    this.dataservice.orderHistory(userId).subscribe(resultorderData => {
      this.spinner.hide();
      console.log(resultorderData);
      if (resultorderData.statusCode == 200) {
        if (resultorderData.responsePacket.walletImage) {
          this.noImg = false;
        }
        this.orderData = resultorderData.responsePacket
      } else {
        this.message = resultorderData;
        localStorage.setItem('statusCode', resultorderData.message);
      }
    },
      error => {

      })
  }

}
