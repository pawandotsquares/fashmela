import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DataService } from "../data.service";
import { Md5 } from "ts-md5/dist/md5";
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  editForm: FormGroup;
  message: object;
  profileDataEdit: object;
  submitted = false;
  profileData: any;
  firstName: any;
  base64textString: any;
  editFromimg: any;
  base64textStringForSave: any;
  imgFlag: boolean;
  noImgFlag: boolean;
  base64ImgFlag: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private FormBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) {
    this.route.params.subscribe(params => this.getprofileDetailsByID(localStorage.getItem('userId')));
   }

  getprofileDetailsByID(id: any) {

    this.DataService.getUserProfile(id).subscribe(resultProfileDetails => {

      var editFormArr = {};
      this.spinner.hide();
      if (resultProfileDetails.statusCode == 200) {
        editFormArr = {
         userId: resultProfileDetails.responsePacket.userId,
         firstName: resultProfileDetails.responsePacket.firstName,
         email: resultProfileDetails.responsePacket.email,
         phoneNumber: resultProfileDetails.responsePacket.phoneNumber,
         address: resultProfileDetails.responsePacket.address,
         profileImage: resultProfileDetails.responsePacket.profileImage
        };
        this.editFromimg = resultProfileDetails.responsePacket.profileImage;
        if (this.editFromimg) {
          this.imgFlag = true;
          this.noImgFlag = false;
          this.base64ImgFlag = false;
        } else {
          this.imgFlag = false;
          this.base64ImgFlag = false;
          this.noImgFlag = true;
        }
        this.editForm.setValue(editFormArr);
        this.profileData = resultProfileDetails.responsePacket;
        this.profileDataEdit = resultProfileDetails.responsePacket
      }
    }

    )
  }

  ngOnInit() {

    this.spinner.show();
    this.editForm = this.FormBuilder.group({
      userId: [''],
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
      profileImage: [''],
      address: ['', Validators.required]
    });

    /* this.DataService.updateUserProfile(this.editForm.value).subscribe(resultProfileData => {
      this.editForm.setValue(resultProfileData.responsePacket);

      }); */
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.editForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }
    this.spinner.show();
    const md5 = new Md5();
    let editFormValue = this.editForm.value;

    //editFormValue.password = md5.appendStr(editFormValue.password).end();

    //call login api here
    this.DataService.updateUserProfile(this.editForm.value, this.base64textStringForSave).subscribe(resultCategories => {
      this.spinner.hide();
      if (resultCategories.statusCode == 200) {
        this.Router.navigateByUrl('/profile');
      }else{
        this.message = resultCategories;
      }
    },
      error => {

      })

  }
  onFileChange(event) {
    var files = event.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);

    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    /* this.base64textString = btoa(binaryString); */
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.base64textStringForSave = btoa(binaryString);
    this.imgFlag = false;
    this.noImgFlag = false;
    this.base64ImgFlag = true;
  }


  onKey(event: any) {

    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
